## About

This is docker image providing REST service for extracting text from PDF and other file types.
It is based on `textract`.

## Usage

_Run docker:_

`docker build --tag=textract .`

`docker run -p 4021:4021 --env PORT=4021 --rm -it textract`

_Test service:_

Open in your browser http://localhost:4021/
Select .pdf file and submit

_Use REST service:_

`POST /extract`

econding: multipart/form-data

provide your file in 'file' variable
