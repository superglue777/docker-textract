const textract = require("textract");
const express = require("express");
const multer = require("multer");

const { sanitizeText } = require("./TextTools");

const uploadService = multer({
  storage: multer.memoryStorage(),
  limits: { fileSize: 1000 * 1000 * 25 }
});

// Hancle Ctrl+C when running in docker interactive session
process.on("SIGINT", function() {
  console.log("Exiting...");
  process.exit();
});

const app = express();
const port = process.env.PORT || 4021;

app.post("/extract", uploadService.single("file"), (req, res) => {
  const { buffer, mimetype, size, originalname } = req.file;
  console.log(
    `Processing file ${originalname}. Size: ${size}. Type: ${mimetype}`
  );
  textract.fromBufferWithMime(mimetype, buffer, (err, text) => {
    if (err) {
      console.error(err);
      res.status(500);
      res.json({ error: err.message });
      return;
    }
    res.json({ text: sanitizeText(text) });
  });
});

app.get("/", (req, res) => {
  res.send(`
    <form action="/extract" method="post" enctype="multipart/form-data">
        <label>Select File</label>
        <input name="file" id="file" type="file">
        <input type="submit" value="submit" />
    </form>  
  `);
});

app.listen(port, () => console.log(`Service is listening on port ${port}!`));
