module.exports = {
  sanitizeText
};

function sanitizeText(text) {
  // TODO: remove lines not containing words (ex. numbers only)

  return text.replace(/-\n/gi, "");
}
