FROM node:10-alpine
RUN npm install -g textract && apk update && apk add bash poppler-utils && mkdir /src
COPY ./src/*.js /src/
COPY ./src/*.json /src/

RUN cd ./src && npm install

CMD ["node", "./src/app.js"]
